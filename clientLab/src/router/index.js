import Vue from 'vue'
import Router from 'vue-router'
import Messages from '@/components/Messages.vue';
import NewMessage from '@/components/NewMessage.vue';
import Signup from '@/components/Signup.vue';
import Signin from '@/components/Signin.vue';
Vue.use(Router)

export default new Router({
  routes: [
  	{ 
  		path: '/', 
  		name: 'Home',
  		component: Messages 
  	},
  	{ 
  		path: '/new',
  		name: 'New-Message', 
  		component: NewMessage
  	},
    { 
      path: '/signup',
      name: 'Signup', 
      component: Signup 
    },
    { 
      path: '/signin',
      name: 'Signin', 
      component: Signin 
    }
  ]
})
