<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use JWTAuth;
use JWTException;

class UserController extends Controller
{
    public function signup(Request $request){
    	$this->validate($request, [
    		'name' => 'required',
    		'email' => 'required|email|unique:users',
    		'password' => 'required'
    	]);

    	User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);

    	return response()->json([
    		'message' => 'Successfully created user!'
    	], 201);
    }

    public function signin(Request $request){
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $credential = $request->only('email','password');
        try {
            if(! $token = JWTAuth::attempt($credential)){
                return response()->json([
                    'error' => 'Invalid credentials'
                ], 401);
            }
        }
        catch(JWTException $e) {
            return response()->json([
                'error' => 'Could not create token!'
            ], 500);
        }
        return response()->json([
            'message' => 'Successfully to login!',
            'token' => $token
        ], 200);
    }
}
